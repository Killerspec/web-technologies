package com.ait.employee;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ConfirmDetailsServlet", urlPatterns = { "/ConfirmDetailsServlet" })
public class ConfirmDetailsServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("Name");
		String age = request.getParameter("Age");
		String salary = request.getParameter("Salary");
		
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		if (name.equals(null)) {
			out.println("<html>");
			out.println("<head><title>Information</title></head>");
			out.println("<h1>");			
			out.println("NO NAME SPECIFIED.");
			out.println("</h1>");
		}else if (age.equals(null)) {
			out.println("<html>");
			out.println("<head><title>Information</title></head>");
			out.println("<h1>");			
			out.println("NO AGE SPECIFIED.");
			out.println("</h1>");
		}else if (salary.equals(null)) {
			out.println("<html>");
			out.println("<head><title>Information</title></head>");
			out.println("<h1>");			
			out.println("NO SALARY SPECIFIED.");
			out.println("</h1>");
		}else {
			out.println("<html>");
			out.println("<head><title>Information</title></head>");
			out.println("<h1>");
			out.println("Hi " +name + " ");
			out.println("you are " + age + " years old, and your salary is ");
			out.println(salary + " euro.");
			out.println("</h1>");
		}
		
		out.flush();
		out.close();
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

