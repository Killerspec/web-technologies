package com.ait.wine;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/wines")
@Stateless
@LocalBean
public class WineWS {
	
	@EJB
	private WineDAO wineDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAll() {
		System.out.println("Get all wines");
		List<Wine> wines = wineDao.getAllWines();
		return Response.status(200).entity(wines).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findWineById(@PathParam("id") int id) {
		Wine wine = wineDao.getWine(id);
		return Response.status(200).entity(wine).build();
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON})
	public Response saveWine(Wine wine) {
		wineDao.save(wine);
		return Response.status(200).entity(wine).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON})	
	public Response updateWine(Wine wine) {
		wineDao.update(wine);
		return Response.status(200).entity(wine).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteWine(@PathParam("id") int id) {
		wineDao.delete(id);
		return Response.status(204).build();
	}
}
